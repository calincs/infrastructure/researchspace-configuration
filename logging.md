The following log levels are available in researchspace:
```        
log4j2.xml
log4j2-debug.xml
log4j2-trace.xml
```

While the exact difference between debug and trace are not yet evident, they each provide more clarity into the underlying mechanism. In particular, `debug` surfaces sparql queries utilized in data transport.

The log level can be set in the docker-compose yaml file by adding the following environmental configurations:
```
-Dlog4j.configurationFile=classpath:org/researchspace/logging/log4j2.xml
-Dlog4j.configurationFile=classpath:org/researchspace/logging/log4j2-debug.xml
-Dlog4j.configurationFile=classpath:org/researchspace/logging/log4j2-trace.xml
```

