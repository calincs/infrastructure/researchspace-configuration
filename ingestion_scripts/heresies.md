Script for loading the heresies dataset into stage. Presupposes loading from a local machine using kubectl. See `data-management.md` for more information about kubectl. The files are copied to storage on the staging cluster, inside blazegraph container. We then send a curl command to BG, asking it to load the files that are in its storage.

```
kubectl cp heresies_may_11/heresies_agents.ttl lincs-rs-custom-app-23837748-staging/blazegraph-655d69c49-xlcjm:/blazegraph-data/heresies_agents.ttl 
kubectl cp heresies_may_11/heresies_iiif.ttl lincs-rs-custom-app-23837748-staging/blazegraph-655d69c49-xlcjm:/blazegraph-data/heresies_iiif.ttl
kubectl cp heresies_may_11/heresies_prices.ttl lincs-rs-custom-app-23837748-staging/blazegraph-655d69c49-xlcjm:/blazegraph-data/heresies_prices.ttl
kubectl cp heresies_may_11/heresies_types.ttl lincs-rs-custom-app-23837748-staging/blazegraph-655d69c49-xlcjm:/blazegraph-data/heresies_types.ttl
kubectl cp heresies_may_11/issue4_lincs.ttl lincs-rs-custom-app-23837748-staging/blazegraph-655d69c49-xlcjm:/blazegraph-data/issue4_lincs.ttl
kubectl cp heresies_may_11/issue6_lincs.ttl lincs-rs-custom-app-23837748-staging/blazegraph-655d69c49-xlcjm:/blazegraph-data/issue6_lincs.ttl
kubectl cp heresies_may_11/issue11_lincs.ttl lincs-rs-custom-app-23837748-staging/blazegraph-655d69c49-xlcjm:/blazegraph-data/issue11_lincs.ttl
kubectl cp heresies_may_11/issue12_lincs.ttl lincs-rs-custom-app-23837748-staging/blazegraph-655d69c49-xlcjm:/blazegraph-data/issue12_lincs.ttl

curl -X POST "https://rs.stage.lincsproject.ca/sparql" -u admin:admin --data-urlencode "update=LOAD <file:/blazegraph-data/heresies_agents.ttl> INTO GRAPH <graph.lincsproject.ca/adArchive/agents>"
curl -X POST "https://rs.stage.lincsproject.ca/sparql" -u admin:admin --data-urlencode "update=LOAD <file:/blazegraph-data/heresies_iiif.ttl> INTO GRAPH <graph.lincsproject.ca/adArchive/iiif>"
curl -X POST "https://rs.stage.lincsproject.ca/sparql" -u admin:admin --data-urlencode "update=LOAD <file:/blazegraph-data/heresies_prices.ttl> INTO GRAPH <graph.lincsproject.ca/adArchive/prices>"
curl -X POST "https://rs.stage.lincsproject.ca/sparql" -u admin:admin --data-urlencode "update=LOAD <file:/blazegraph-data/heresies_types.ttl> INTO GRAPH <graph.lincsproject.ca/adArchive/types>"
curl -X POST "https://rs.stage.lincsproject.ca/sparql" -u admin:admin --data-urlencode "update=LOAD <file:/blazegraph-data/issue4_lincs.ttl> INTO GRAPH <graph.lincsproject.ca//adArchive/issue4>"
curl -X POST "https://rs.stage.lincsproject.ca/sparql" -u admin:admin --data-urlencode "update=LOAD <file:/blazegraph-data/issue6_lincs.ttl> INTO GRAPH <graph.lincsproject.ca//adArchive/issue6>"
curl -X POST "https://rs.stage.lincsproject.ca/sparql" -u admin:admin --data-urlencode "update=LOAD <file:/blazegraph-data/issue11_lincs.ttl> INTO GRAPH <graph.lincsproject.ca/adArchive/issue11>"
curl -X POST "https://rs.stage.lincsproject.ca/sparql" -u admin:admin --data-urlencode "update=LOAD <file:/blazegraph-data/issue12_lincs.ttl> INTO GRAPH <graph.lincsproject.ca/adArchive/issue12>"
```

The following ontologies require loading, but we'll wait to load them when we find necessary. Alternatively, these may be more suited to vocabulary manager:
```
https://id.loc.gov/ontologies/bibframe.rdf
https://www.dublincore.org/specifications/bibo/bibo/bibo.rdf.xml
http://www.cidoc-crm.org/sites/default/files/CRMpc_v1.1_0.rdfs #isn't loading, but this represents very few expressions


'''

The following namespaces are potentially required in RS subsystem, if we intend to query them shorthand:
'''
@prefix bf: <http://id.loc.gov/ontologies/bibframe/> .
@prefix bibo: <http://purl.org/ontology/bibo/> .
@prefix lincs: <http://id.lincsproject.ca/> .
@prefix locinsts: <http://id.loc.gov/resources/instances/> .
@prefix locnames: <https://id.loc.gov/authorities/names/> .
@prefix locprovs: <http://id.loc.gov/entities/providers/> .
@prefix locworks: <https://id.loc.gov/resources/works/> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix schema: <https://schema.org/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix viaf: <http://viaf.org/viaf/> .
@prefix wikidata: <http://www.wikidata.org/entity/> .
@prefix worldcat: <http://worldcat.org/entity/work/id/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
```