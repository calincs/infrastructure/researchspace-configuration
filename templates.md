
Search
 * [Query](https://rs.sandbox.lincsproject.ca/resource/:SearchContent?action=edit)
   *  `<bs-tab>` new search tab (add as many as you need)
   * `<semantic-search>` query definition
     * `domain` set to capture knowledge patterns corresponding to this domain (for facets)
     * `query` 
       * set the superclass to the type of entity you want driving search.
       * set the label predicate to whatever suits the dataset (rdfs/skos/dc).

Search results
 * [Results](https://rs.sandbox.lincsproject.ca/resource/:SearchResults?action=edit)
   * `<semantic-search-facet>` faceted search settings
   * `<semantic-search-result-holder>` search results
   * `<bs-tab>`
     * `<semantic-grid>` (default)
     * `<semantic-table>`
     * `<semantic-timeline>`
     * `<semantic-chart>`

   


The following templates can be edited from the browser or in `./src/main/resources/org/researchspace/apps/default/data/templates`:

ontodia.org/schema/v1#Diagram.html
www.cidoc-crm.org/cidoc-crm/E1_CRM_Entity.html
www.cidoc-crm.org/cidoc-crm/E24_Physical_Man-Made_Thing.html
www.cidoc-crm.org/cidoc-crm/E32_Authority_Document.html
www.researchspace.org/ontology/EX_Digital_Image_Region.html
www.researchspace.org/ontology/Semantic_Narrative.html
www.researchspace.org/ontology/User.html
www.w3.org/2004/02/skos/core#ConceptScheme.html
AuthorityConfigForm.html
Clipboard.html
ClipboardConfig.html
ClipboardKnowledgeMapView.html
ClipboardSidebar.html
CollectionBrowserTemplate.html
DetailsSidebar.html
EntityEditor.html
FormDefaultActions.html
HierarchyManagerTemplate.html
IIIFConfig.html
ImageAnnotationForm.html
ImageAnnotationObservationDocumentation.html
ImageAnnotationObservationInput.html
ImageAnnotationWorkspace.html
ItemCardFooter.html
ItemCardMedia.html
KnowledgeMapOntodiaConfig.html
ProjectConfiguration.html
ProjectDashboard.html
ProjectImages.html
ProjectKnowledgeMaps.html
ProjectNarratives.html
ProjectUsers.html
ResourceContent.html
ResourceFieldBasedVisualization.html
ResourceFieldValueVisualization.html
ResourceTemplate.html
SearchContent.html
SearchResults.html
SetGridView.html
SetListView.html
SetView.html
SimpleCollectionBrowser.html
Start.html
ThinkingFrames.html
ThinkingFramesFeatureSimilarityKm.html
ThinkingFramesIIIFTemplate.html
ThinkingFramesKnowledgeMapTemplate.html
ThinkingFramesObjectThroughImageObservation.html
ThinkingFramesSemanticNarrativeTemplate.html
TypeMappings.html
VocabularyManager.html
itemCardTemplate.html
itemCardTemplateContent.html
itemCardTemplateDefault.html
E4_Period-Ancient_Period.html
crm:E1_CRM_Entity.html
crm:E21_Person.html
crm:E4_Period.html
crm:E53_Place.html
skos:Concept.html
authority_manager_config_types.html
userInfoForm.html
userProfileForm.html
userProjectsForm.html