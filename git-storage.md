## Git Storage
<br>

#### Store template changes with git versioning enabled. Stores changes locally and automatically pushes them to remote.
#### Adapted from this [Metaphacts Tutorial](https://help.metaphacts.com/resource/Help:GitStorageGuide).

<br>

1. Under the docker-compose template folder (ie. `researchspace-docker-compose/basic`), install a folder called `git-keys`.

2. Create an ssh-key, share the public key with your remote git account, and save the private key to the `git-keys` folder.
```
> cd git-keys
> ssh-keygen -t rsa -N "" -m PEM -f id_rsa -C user@mymail.com
```
3. Initialize a new repo and clone it to your docker-compose template folder (ie. `researchspace-docker-compose/basic/rs-storage`).

4. In the `git-keys` folder, create file `config`.
```
Host github.com
  HostName github.com
  User user@mymail.com
  IdentityFile ~/.ssh/id_rsa
```

5. In the `git-keys` folder, generate a known_hosts file from cli.
```
> cd git-keys
> ssh-keyscan github.com >> known_hosts
```

6. Edit the `.env` file. Notes: Replace `git@github.com:linkedDataDeveloper/rs-storage.git` with your remote endpoint. As of January 2020, github uses 'main' instead of 'master' for the default branch (ie. `-Dconfig.storage.runtime.branch=main`).
```
RESEARCHSPACE_OPTS=-Dorg.eclipse.jetty.server.Request.maxFormContentSize=1000000 -Dconfig.storage.runtime.type=git -Dconfig.storage.runtime.mutable=true -Dconfig.storage.runtime.localPath=/git-runtime-data -Dconfig.storage.runtime.branch=main -Dconfig.storage.runtime.remoteUrl=git@github.com:linkedDataDeveloper/rs-storage.git
```
7. Edit the `volumes` section of `docker-compose.yaml`. Remove `-./researchspace/runtime-data:/runtime-data`. Note `rs-storage` is the name of the locally created git repo.
```
# runtime data folder with config files and ad-hoc templates 
- ./rs-storage:/git-runtime-data # your checked out repo as runtime folder
- ./git-keys:/home/jetty/.ssh/  # your private key, known_hosts, and config file 
```

8.  From the command line, run `Docker-compose up -d` and fix file permissions with the following:

```
sudo docker exec -u root lincs_basic_git_researchspace_1 sh -c "chown -R jetty:root /git-runtime-data"
sudo docker exec -u root lincs_basic_git_researchspace_1 sh -c "chown -R jetty:root /home/jetty/.ssh/ && chmod -R go-rwx /home/jetty/.ssh/"
```

Git storage should now be activated.