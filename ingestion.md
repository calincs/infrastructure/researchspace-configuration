Ingestion workflow

1. After data has been quality tested by conversion pipeline, it should arrive at a designated storage location (S3).
   * Storage location should be accessible to conversion and publishing people.
   * Storage should have policies indicating folder structure, naming conventions, and versioning.
   * Any supporting documentation to help with step 3 should also be included alongside the data.

2. [Load data](https://gitlab.com/calincs/infrastructure/researchspace-configuration/-/blob/master/data-management.md)

3. Run analysis over new data.
   * identify namespaces (vocabularies and ontologies)
   * add supporting ontologies or vocabularies to RS where applicable
   * identify primary classes of interest (i.e. event, person, place, thing)
   * identify all incoming and outgoing predicates for major classes

4. Load ontologies into ResearchSpace that are not currently present in the system

5. Add namespace identifiers to the system
 * Currently doing this through the admin console
 * Figure out how to preload namespaces into the system (so we can reproduce this without reloading)
   
4. Define default search query at https://rs.sandbox.lincsproject.ca/resource/:SearchContent?action=edit

5. Set knowledge patterns for faceted search

6. Set knowledge patterns for resource view

7. Mint a new user to supply the data owner with read/write privs.

8. Book a walkthrough with the data owner to view data and discuss research goals.
 
