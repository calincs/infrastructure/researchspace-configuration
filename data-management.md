### Data Storage

Small data files can be uploaded to Researchspace directly through the admin interface (https://rs.sandbox.lincsproject.ca/resource/Admin:DataImportExport). However, for larger data files, [we can use an API to handle to the load.](https://gitlab.com/calincs/conversion/NSSI/-/wikis/Usage/Ingestion) 