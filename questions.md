1) Search facets only appear when a knowledge pattern domain matches the default domain in the search template. Facets only appear when the knowledge pattern contains no more than one domain, and it needs to be the exact same domain (not child classes). For example, if I have more than one domain given to a knowledge pattern, it does not appear. If the domain of a knowledge pattern is 

2) Why are some knowledge patterns categorized into either long random strings, or into authority manager? Why doesn't the category tree populate? How do I get it to populate? Where do I create categories?

3) Why does "Universal Drag and Drop Entity Edit" return a blank screen? Navigating from

3) LDP containers and graph names, how do they work together, how can we leverage permissions over LDP? I need a better understanding of how LDP functions. Why are they generated? When? How can they be used?
https://help.metaphacts.com/resource/Help:LDPAssetsManagement

4) Should probably fork the configuration containers and edit them to suit our needs



Demo:

documentation.researchspace.org

-data import
    - enriching data with images
    - went to wikidata built a construct statement, converted to trig file within its own named graph
        - gives you finer control over named graphs within a single file
    - requirements for naming graph. loading and reloading graphs.
    - named graph per data set until data gets really, really huge.


Templates (16mins)

    -rely on default templates
    -define knowledge patterns
    -there is a template for every CRM_entity
    -look for cipboardsidebar, detailsidebar
        -resource content (inserted template). look at mp-field-visualization. Takes a list of knowledge pattern fields and the resource and injects them into the template. [[this]] is the resource (i.e. subject). does a select statement on all fields.
    -when making knowledge patterns, use the duplicate key.
    -knowledge pattern can traverse over the graph to get related entities across more than one triple
    -resourcefieldvaluevisualization is a template for each row in a data representation

Semantic search (second recording)
    SearchContent template
    <semantic-search-query-keyword>
        domain='knowedge pattern link'
        forces a particular resource to appear in search

    itemCardTemplate

Categorgies
    <semantic-search config=>    
    define knowledge patterns by category. use category manager under config (yet to be merged) to create and manage categories. Assign a knowledge pattern to a category. You can then filter queries by that category to make views specific to a category of knowledge patterns. ideal for different user types: a conservationist vs a curator, for example.

Event system
    -may become useful one day
    -can pass data between components. update based on form submission, etc.
