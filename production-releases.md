When releasing a new or updated version of LINCS researchspace to production, some protocols need to be followed.

1. [LINCS custom app](https://gitlab.com/calincs/infrastructure/lincs-rs-custom-app) needs to be deployed to production (manually pushed from the CI interface). The lincs custom app will contain saved template pages and application configs that were developed in stage.

2. Data can be ingested, on a graph by graph basis, through the RS UI or the NSSI API. Just be sure you are using the same named graph IRIs used in stage. [See Data Management for instructions.](https://gitlab.com/calincs/infrastructure/researchspace-configuration/-/blob/master/data-management.md) 

3. Knowledge patterns can be loaded by extracting the KP catalog (an LDP container) from stage and importing into prod as an LDP container. The extract will contain all existing KPs including the default KPs. When we want to update prod, we replace the prod LDP container with a fresh extract.

4. Authority and Entity configurations are created manually in the stage admin page. I am currently working on a way to extract those configurations. TBD.
